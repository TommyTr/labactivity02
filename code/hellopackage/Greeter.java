package hellopackage;

import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

public class Greeter{

    public static void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        java.util.Random rand=new java.util.Random();

        System.out.println("Enter a number");
        int num=scanner.nextInt();
        
        Utilities utilities=new Utilities();

        int result=utilities.doubleMe(num);
        System.out.println("After doubleMe: "+result);
    }
}